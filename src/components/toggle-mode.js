import React from "react"
import styled, { ThemeConsumer } from "styled-components"
import { styles } from "./styles"
import SunIcon from "../images/sun.inline.svg"
import StarIcon from "../images/star.inline.svg"

const Button = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  height: 45px;
  width: 45px;
  padding: 5px;
  position: relative;
  background: transparent;
  border: 0;
  cursor: pointer;
  margin-right: 15px;

  &:focus {
    outline-color: ${props =>
      props.theme === "dark" ? styles.colorDark : styles.colorSecondary};
  }

  svg {
    width: 100%;

    path {
      fill: #fff;
      transition: fill 0.3s ease;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${props =>
          props.theme === "dark" ? styles.colorDark : styles.colorSecondary};
      }
    }
  }
`

export default function ToggleMode() {
  return (
    <ThemeConsumer>
      {theme => (
        <Button
          className="toggle-mode"
          theme={theme.mode}
          onClick={e =>
            theme.setTheme(
              theme.mode === "dark"
                ? { ...theme, mode: "light" }
                : { ...theme, mode: "dark" }
            )
          }
        >
          {theme.mode === "dark" ? <StarIcon /> : <SunIcon />}
        </Button>
      )}
    </ThemeConsumer>
  )
}
