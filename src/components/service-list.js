import React from "react"
import { withStyles } from "@material-ui/core/styles"
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel"
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary"
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails"
import { styles, Text } from "./styles"
import styled from "styled-components"
import Fade from "react-reveal/Fade"
import parse from "html-react-parser"
import { hyphenate, hyphenateSync } from "hyphen/pl"

const List = styled.div`
  padding-left: 15px;
  position: relative;

  @media (min-width: 600px) {
    padding-left: 85px;
  }

  &::after {
    content: "";
    height: 100%;
    width: 4px;
    position: absolute;
    top: 0;
    left: 0;
    background-color: ${styles.colorSecondary};

    @media (min-width: 600px) {
      left: 40px;
    }

    @media (min-width: 992px) {
      left: 0;
    }
  }

  .react-reveal {
    @media (min-width: 768px) {
      ${props => getAnimations(props.services)};
    }
  }

  .MuiExpansionPanelSummary-root {
    padding: 0;

    .Mui-expanded {
      border: 1px solid ${styles.colorSecondary};
    }
  }

  .MuiExpansionPanelSummary-content {
    border: 1px solid transparent;
    transition: border-color 0.3s ease;
    padding: 20px 12px;
    margin: 0;

    &:hover {
      border: 1px solid ${styles.colorSecondary};
    }

    p {
      font-weight: 900;
    }
  }

  .MuiExpansionPanelDetails-root {
    padding: 10px 12px 25px 12px;
    background-color: rgba(193, 160, 72, 0.08);

    p {
      color: #000;
    }
  }
`

const ExpansionPanel = withStyles({
  root: {
    boxShadow: "none",
    "&:before": {
      display: "none",
    },
    "&$expanded": {
      margin: "auto",
    },
  },
  expanded: {},
})(MuiExpansionPanel)

const ExpansionPanelSummary = withStyles({
  content: {
    margin: "0",
    //     '&$expanded': {
    //       margin: '12px 0',
    //     },
  },
  expanded: {},
})(MuiExpansionPanelSummary)

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiExpansionPanelDetails)

export default function CustomizedExpansionPanels(props) {
  const [expanded, setExpanded] = React.useState("")
  const services = props.services
  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false)
  }

  // const handleClick = panel => () => {
  //   setExpanded(expanded ? panel : false);
  // };

  return (
    <List services={services}>
      {services.map((item, index) => {
        index += 1
        const content = hyphenateSync(item.opis_uslugi)
        return (
          <Fade>
            <ExpansionPanel
              square
              expanded={expanded === `panel${index}`}
              onChange={handleChange(`panel${index}`)}
              //onClick={handleClick(`panel${index}`)}
            >
              <ExpansionPanelSummary
                aria-controls={`panel${index}d-content`}
                id={`panel${index}d-header`}
              >
                <Text color={styles.colorPrimary}>{item.nazwa_uslugi}</Text>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Text color={styles.colorPrimary} align={"justify"}>
                  {parse(content)}
                </Text>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Fade>
        )
      })}
    </List>
  )
}

function template(i, duration) {
  duration += 50 * i
  return `
        &:nth-child(${i + 1}) {
          animation-delay: ${duration}ms !important;
         }
      `
}
const getAnimations = services => {
  let str = ""
  const duration = 300

  for (let index = 0; index < services.length; index += 1) {
    str += template(index, duration)
  }
  return str
}
