import React from "react"
import { Link } from "gatsby"
import styled, { ThemeConsumer } from "styled-components"
import { styles } from "./styles"
import Fade from "react-reveal/Fade"
import ToggleMode from "./toggle-mode"

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  height: 67px;
  position: relative;

  button.active ~ ul {
    transform: translateX(0);
    display: flex;
    position: absolute;
  }

  & > a {
    margin-right: 20px;

    &:nth-last-of-type(1) {
      margin-right: 15px;
    }
  }
`

const List = styled.ul`
  display: none;
  flex-direction: column;
  list-style: none;
  padding: 15px;
  position: absolute;
  top: 90px;
  right: 0;
  transform: translateX(-100%);
  transition: transform 0.3s ease;
  margin: 0;

  li {
    &:nth-child(1) {
      .react-reveal {
        animation-delay: 100ms !important;
      }
    }

    &:nth-child(2) {
      .react-reveal {
        animation-delay: 150ms !important;
      }
    }

    &:nth-child(3) {
      .react-reveal {
        animation-delay: 200ms !important;
      }
    }

    &:nth-child(4) {
      .react-reveal {
        animation-delay: 250ms !important;
      }
    }

    &:nth-child(5) {
      .react-reveal {
        animation-delay: 300ms !important;
      }
    }
  }

  @media (min-width: 992px) {
    padding: 0;
    position: static;
    transform: translateX(0);
  }
`

const Hamburger = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  height: 45px;
  width: 45px;
  padding: 5px;
  position: relative;
  background: transparent;
  border: 0;
  cursor: pointer;

  &:focus {
    outline-color: ${props =>
      props.theme === "dark" ? styles.colorDark : styles.colorSecondary};
  }

  span {
    display: block;
    height: 7px;
    width: 7px;
    background-color: #fff;
    border-radius: 2px;
    position: relative;
    transition: background-color 0.3s ease;

    &::before,
    &::after {
      content: "";
      height: 7px;
      width: 7px;
      background-color: inherit;
      border-radius: 2px;
      position: absolute;
    }

    &::before {
      left: -14px;
    }

    &::after {
      right: -14px;
    }
  }

  &:hover {
    span {
      background-color: ${props =>
        props.theme === "dark" ? styles.colorDark : styles.colorSecondary};
    }
  }

  &.active {
    justify-content: center;
    padding: 2px;

    span {
      position: absolute;
      transition: transform 0.3s ease;

      &:nth-child(1) {
        top: calc(50% - 3px);
        transform: rotate(45deg);
        transform-origin: center;
      }

      &:nth-child(2) {
        display: none;
      }

      &:nth-child(3) {
        bottom: calc(50% - 3px);
        transform: rotate(-45deg);
        transform-origin: center;
      }
    }
  }
`

const MenuItem = styled.li`
  display: flex;
  justify-content: flex-end;
`

const MenuLink = styled(Link)`
  display: flex;
  align-items: center;
  width: max-content;
  color: ${props =>
    props.theme === "dark" ? styles.colorDark : styles.colorSecondary};
  background-color: #fff;
  border: 1px solid #fff;
  font-family: "Oswald", sans-serif;
  font-size: 15px;
  line-height: 1;
  font-weight: 400;
  text-align: center;
  padding: 8px 23px;
  text-transform: uppercase;
  text-decoration: none;
  position: relative;
  transition: color 0.2s ease, background-color 0.3s ease,
    border-color 0.3s ease;

  &:hover {
    color: #fff;
    background-color: ${props =>
      props.theme === "dark" ? styles.colorDark : styles.colorSecondary};
    border-color: ${props =>
      props.theme === "dark" ? styles.colorDark : styles.colorSecondary};
  }
`

class HomePageMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hamburgerOpen: false,
    }
  }

  handleClick = e => {
    e.target.closest("button").classList.toggle("active")
    this.setState(prevState => ({
      hamburgerOpen: !prevState.hamburgerOpen,
    }))
  }

  handleMenuItemClick = e => {
    e.target.closest("ul").previousSibling.classList.remove("active")
  }

  componentDidMount = () => {
    const hero = document.querySelector(".hero")
    const header = document.querySelector(".header")

    if (hero) {
      const heroSize = document.querySelector(".hero").offsetHeight - 110

      if (document.documentElement.scrollTop > heroSize) {
        header.classList.add("alt")
      } else {
        header.classList.remove("alt")
      }

      window.onscroll = () => {
        if (document.documentElement.scrollTop > heroSize) {
          header.classList.add("alt")
        } else {
          header.classList.remove("alt")
        }
      }
    } else {
      header.classList.add("alt")
    }
  }

  render() {
    return (
      <ThemeConsumer>
        {theme => (
          <Wrapper className={"menu"}>
            {/* <ToggleMode /> */}
            <Hamburger onClick={e => this.handleClick(e)} theme={theme.mode}>
              <span />
              <span />
              <span />
            </Hamburger>
            <List>
              <MenuItem>
                <Fade>
                  <MenuLink
                    to={"/"}
                    onClick={e => this.handleMenuItemClick(e)}
                    theme={theme.mode}
                  >
                    Home
                  </MenuLink>
                </Fade>
              </MenuItem>
              <MenuItem>
                <Fade>
                  <MenuLink
                    to={"#o-kancelarii"}
                    onClick={e => this.handleMenuItemClick(e)}
                    theme={theme.mode}
                  >
                    O kancelarii
                  </MenuLink>
                </Fade>
              </MenuItem>
              <MenuItem>
                <Fade>
                  <MenuLink
                    to={"#uslugi-prawne"}
                    onClick={e => this.handleMenuItemClick(e)}
                    theme={theme.mode}
                  >
                    Usługi prawne
                  </MenuLink>
                </Fade>
              </MenuItem>
              <MenuItem>
                <Fade>
                  <MenuLink
                    to={"blog"}
                    onClick={e => this.handleMenuItemClick(e)}
                    theme={theme.mode}
                  >
                    Aktualności
                  </MenuLink>
                </Fade>
              </MenuItem>
              <MenuItem>
                <Fade>
                  <MenuLink
                    to={"#kontakt"}
                    onClick={e => this.handleMenuItemClick(e)}
                    theme={theme.mode}
                  >
                    Kontakt
                  </MenuLink>
                </Fade>
              </MenuItem>
            </List>
          </Wrapper>
        )}
      </ThemeConsumer>
    )
  }
}

export default HomePageMenu
