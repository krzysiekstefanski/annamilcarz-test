module.exports = {
  siteMetadata: {
    title: `annamilcarz.pl`,
    description: `Radca prawny Anna Milcarz.`,
    author: `Krzysztof Stefański`,
    siteUrl: `https://www.radca-annamilcarz.pl`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-source-wordpress",
      options: {
        baseUrl: "kstefanski.pl/annamilcarz/",
        protocol: "http",
        hostingWPCOM: false,
        useACF: true,
        auth: {
          htaccess_user: "admin",
          htaccess_pass: "woa19hs89",
        },
        includedRoutes: [
          "**/categories",
          "**/posts",
          "**/pages",
          "**/media",
          "**/tags",
          "**/taxonomies",
          "**/users",
          "**/menu-locations/**",
          "**/menus",
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `strona-annamilcarz`,
        short_name: `annamilcarz`,
        start_url: `/`,
        background_color: `#FFF`,
        theme_color: `#79768b`,
        display: `minimal-ui`,
        icon: `src/images/annamilcarz-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-material-ui`,
      options: {
        stylesProvider: {
          injectFirst: true,
        },
      },
    },
    {
      resolve: "gatsby-plugin-web-font-loader",
      options: {
        google: {
          families: ["Lato:300,400,700,900:latin,latin-ext"],
        },
      },
    },
    {
      resolve: `gatsby-styled-components-dark-mode`,
      options: {
        //light: require(`${__dirname}/src/components/styles.js`).styles,
        //dark: require(`${__dirname}/src/components/styles.js`).stylesDark,
        light: { mainColor: "#111e2f" },
        dark: { mainColor: "#000" },
      },
    },
    `gatsby-plugin-svgr-svgo`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-sitemap`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
